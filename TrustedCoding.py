import uuid
import json
import sublime
import mdpopups
import threading
import sublime_plugin
from datetime import datetime
from time import sleep
import requests
import webbrowser




CLOSE_BUTTON = '\n\n<p>\n<a href="#" class="btn btn-small btn-info">Cancel</a>\n</p>'
HIDE_BUTTON = '\n\n<p>\n<a href="{id}" class="btn btn-small btn-info">Hide</a>\n</p>'
mapping_risk = {"CRITICAL": "error", "MEDIUM": "warning", "LOW": "info", "HIGH": "error"}
VULN_TEXT = '''
!!! panel-{mapped} "{risk}"
    <i>Line:{line}</i><br>{message} 
'''

VULN_TEXT_FULLCODE =  '''
    <b>{language}</b>
    ```    
    {fullcode}
    ```
'''

VULN_TEXT_REFERENCES =  '''
    <b>References</b>
     
    {references}
    
'''


Min_lvl_Scan=1
Max_lvl_Scan=1

def parse_call(self, code):
    language = mdpopups.get_language_from_view(self.view)
    if language not in [ "php" , "python" ]:
        sublime.message_dialog("Language must be PHP or Python: '" + language + "' detected!")
        return

    self.job = AnalysisApiCall(code=code, references=self.references, depth=self.depth, language=language, timeout=240, view=self.view, user=self.user, tool=self.tool)
    self.job.start()
    sleep(0.3)

def on_cancel():
    print("Analysis canceled")

class TrustedCodingCommand(sublime_plugin.TextCommand):
    def run(self, edit):
        self.stop = False
        self.user=""
        code = self.view.substr(sublime.Region(0, self.view.size()))
        if len(code)<=0:
            sublime.error_message("Click on the code to analyze")
            return
        File = False
        try:
            my_set = sublime.load_settings("Trustedcoding.sublime-settings")
            self.user=my_set.get('user')
            if self.user:
                self.references=my_set.get('references')
                self.depth=my_set.get('depth')
                self.tool=my_set.get('tool')
                self.time=my_set.get('time')
                self.experience=my_set.get('experience')
            else:
                print("User not found, creating...")
                self.user=str(uuid.uuid4())
                self.references=False
                self.depth=False
                self.opener=False
                self.tool=[]
                self.time=False
                self.experience=0
                my_set.set('user', self.user)
                my_set.set('references', self.references)
                my_set.set('depth', self.depth)
                my_set.set('tool', self.tool)
                my_set.set('time', self.time)
                my_set.set('experience', self.experience)
                sublime.save_settings("Trustedcoding.sublime-settings")

        except Exception as e:
            print(str(e))
            File = False

        parse_call(self, code)  
    
    def on_close_popup(self, href):
        opener = sublime.load_settings("Trustedcoding.sublime-settings").get('opener')
        if opener:
            if href == '#':
                self.job.stop = True
                mdpopups.hide_popup(self.n_v)
                self.n_v.set_read_only(False)
        else:
            if href == '#':
                self.job.stop = True
                mdpopups.hide_popup(self.view)
                self.view.set_read_only(False)


    def on_hide_popup(self):
        opener = sublime.load_settings("Trustedcoding.sublime-settings").get('opener')
        
        if self.job.is_alive() and not self.job.stop:
            if opener:
                mdpopups.show_popup(self.view, LOADING_PAGE + CLOSE_BUTTON, location=self.view.visible_region().a, on_navigate=self.on_close_popup, on_hide=self.on_hide_popup, max_height=650, max_width=600, wrapper_class='mdpopups-test', css='div.mdpopups-test { padding: 0.5rem; }')
            else:
                mdpopups.show_popup(self.n_v, LOADING_PAGE + CLOSE_BUTTON, location=self.n_v.visible_region().a, on_navigate=self.on_close_popup, on_hide=self.on_hide_popup, max_height=650, max_width=600, wrapper_class='mdpopups-test', css='div.mdpopups-test { padding: 0.5rem; }') 
        else:
            self.view.set_read_only(False)

class SetOpenerCommand(sublime_plugin.ApplicationCommand ):
    opener = False
    def run(self):
        self.opener = not self.opener
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        my_set.set('opener', self.opener)
        sublime.save_settings("Trustedcoding.sublime-settings")

    def is_checked(self):
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        return sublime.load_settings("Trustedcoding.sublime-settings").get('opener', self.opener)
        
class SetReferencesCommand(sublime_plugin.ApplicationCommand ):
    references = False
    def run(self, **args):
        self.references = not self.references
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        my_set.set('references', self.references)
        sublime.save_settings("Trustedcoding.sublime-settings")

    def is_checked(self):        
        return sublime.load_settings("Trustedcoding.sublime-settings").get('references', self.references)

class SetDepthCommand(sublime_plugin.ApplicationCommand ):
    depth = False
    def run(self, **args):
        self.depth = not self.depth
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        my_set.set('depth', self.depth)
        sublime.save_settings("Trustedcoding.sublime-settings")

    def is_checked(self):                
        return sublime.load_settings("Trustedcoding.sublime-settings").get('depth')

class SetTimeCommand(sublime_plugin.ApplicationCommand ):
    time = False
    def run(self, **args):
        self.time = not self.time
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        my_set.set('time', self.time)
        sublime.save_settings("Trustedcoding.sublime-settings")

    def is_checked(self):                
        return sublime.load_settings("Trustedcoding.sublime-settings").get('time', self.time)

class SetExperienceCommand(sublime_plugin.ApplicationCommand ):
    experience = False
    def run(self, **args):
        self.experience = not self.experience
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        my_set.set('experience', self.experience)
        sublime.save_settings("Trustedcoding.sublime-settings")

    def is_checked(self):                
        return sublime.load_settings("Trustedcoding.sublime-settings").get('experience', self.experience)

class ArgumentInputHandler(sublime_plugin.TextInputHandler):
    def placeholder(self):
        return ""

class AnalysisApiCall(threading.Thread):
    def __init__(self, code, references, depth, language, timeout, view, user, tool):
        self.code = code
        self.language = language
        self.timeout = timeout
        self.user = user
        self.references = references
        self.depth = depth
        self.stop = False
        self.view = view
        self.tool = tool
        self.script_time = None
        self.script_time_done = False
        self.script_time_cancel = False
        self.experience_time = None
        self.experience_time_done = False
        self.experience_time_cancel = False
        threading.Thread.__init__(self)        
    
    def run(self):
        try:
            err = ''
            my_set = sublime.load_settings("Trustedcoding.sublime-settings")
            opener = my_set.get('opener')
            time = my_set.get('time')
            experience = my_set.get('experience')
                  
            if time:
                self.view.window().show_input_panel( 
                    "Scripting Time (minutes)", 
                    "0", 
                    self.time_on_done, 
                    None, 
                    self.time_on_cancel
                )
                while not self.script_time_done:
                    sleep(1)
                    print("Waiting user's input...!")

                if self.script_time_cancel:
                    print("Canceled...!")
                    return
            else:
                self.script_time = 0

            if experience:
                self.view.window().show_input_panel( 
                    "Experience Time (years)", 
                    "0", 
                    self.experience_on_done, 
                    None, 
                    self.experience_on_cancel
                )
                while not self.experience_time_done:
                    sleep(1)
                    print("Waiting user's input...!")

                if self.experience_time_cancel:
                    print("Canceled...!")
                    return
            else:
                self.experience_time = 0

            data = {
                "levels": [], 
                "source": self.code, 
                "language": self.language,
                "user":self.user,
                "depth":self.depth,
                "references":self.references,
                "experience" : self.experience_time,
                "time" : self.script_time
            }

            url = "https://trustedcoding.pre.inftel.madisonmk.com/api/v1/sast/code"
            
            response = requests.request('POST', url=url, json=data)
            result = response.json()

            phantoms = []
            p = -1
            if self.stop:
                return
            else:
                self.stop = True 
                if opener:
                    sublime.message_dialog("End of analysis")
                    self.n_v = sublime.active_window().new_file()
                    self.n_v.set_name("TrustedCoding [" + str(datetime.now()) + "]")
                    sublime.active_window().focus_view(self.view)
                    mdpopups.hide_popup(self.n_v)
                else:
                    sublime.message_dialog("End of analysis")
                    mdpopups.hide_popup(self.view)

                for vuln in result:
                    line = int(vuln["line"])
                    severity = vuln["severity"]
                    
                    popup = VULN_TEXT
                    if vuln["fullcode"] is not None:
                        fullcode = '\n\t'.join(vuln["fullcode"].split('\n'))
                        popup = popup + VULN_TEXT_FULLCODE 
                    else:
                        fullcode = ''

                    refs = vuln.get('references', [])
                    if len(refs) > 0:
                        popup = popup + VULN_TEXT_REFERENCES 
                        references = ''
                        for r in refs:
                            references = references + "methodology: " +  r.get('methodology') + " <br>" #'REFERENCES YES'#'\n\t'.join(vuln["fullcode"].split('\n')) 
                            references = references + r.get('methodology') + " ID: "  + r.get('internal_id') + " <br>" #'REFERENCES YES'#'\n\t'.join(vuln["fullcode"].split('\n')) 
                            references = references + "name: <a target='_blank' href='" + r.get('link') + "'>" +  r.get('name') + "</a> <br>" #'REFERENCES YES'#'\n\t'.join(vuln["fullcode"].split('\n')) 
                            references = references + "URL: <a target='_blank' href='" + r.get('link') + "'>" +  r.get('link') + "</a> <hr>" #'REFERENCES YES'#'\n\t'.join(vuln["fullcode"].split('\n'))                  
                    else:
                        references = ''
                    
                    message = vuln["message"]
                    lines = self.code.split("\n")
                    chars = 0
                    for i in range(0, line):
                        chars += len(lines[i])
                    region = sublime.Region(chars, chars)
                    id = uuid.uuid4().hex
                    phantoms.append(id)
                    if opener:
                        mdpopups.add_phantom(
                            self.n_v, id, region, popup.format(mapped=mapping_risk[severity], risk=severity.upper(), message=message, language=self.language, fullcode=fullcode, references=references, line=line) + HIDE_BUTTON.format(id=id), 2,
                            on_navigate=self.on_close_phantom, wrapper_class='mdpopups-test')
                        my_view = self.n_v
                    else:
                        mdpopups.add_phantom(
                            self.view, id, region, popup.format(mapped=mapping_risk[severity], risk=severity.upper(), message=message, language=self.language, fullcode=fullcode, references=references, line=line) + HIDE_BUTTON.format(id=id), 2,
                            on_navigate=self.on_close_phantom, wrapper_class='mdpopups-test')
                        my_view = self.view
                    
                self.view.set_read_only(False)
                my_set = sublime.load_settings("Trustedcoding.sublime-settings")
                my_set.set('phantoms', phantoms)
                my_set.set('my_view', my_view)
        except Exception as e:
            print(e)
            err = 'Unkown exception'
        if not self.stop:
            sublime.error_message(err)

    def on_close_phantom(self, href):
        try:
            mdpopups.erase_phantoms(self.n_v, href)
        except AttributeError as e:
            mdpopups.erase_phantoms(self.view, href)
        except Exception as e: 
            print(e)
    
    def time_on_done(self, user_input):
        try:
            self.script_time = int(user_input)
        except:
            self.script_time = 0

        self.script_time_done = True

    def time_on_cancel(self):
        self.script_time_done = True
        self.script_time_cancel = True

    def experience_on_done(self, user_input):
        try:
            self.experience_time = int(user_input)
        except:
            self.experience_time = 0

        self.experience_time_done = True

    def experience_on_cancel(self):
        self.experience_time_done = True
        self.experience_time_cancel = True

class ClearResultsCommand (sublime_plugin.TextCommand ):

    def run(self, edit):
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        phantoms = my_set.get('phantoms')
        my_view = my_set.get('my_view')

        for href in phantoms:       
            try:
                mdpopups.erase_phantoms(self.view, href)            
            except Exception as e: 
                print(e)

class OpenBrowserCommand (sublime_plugin.TextCommand ):

    def run(self, edit):
        my_set = sublime.load_settings("Trustedcoding.sublime-settings")
        report_uri = "https://trustedcoding-ai.pre.inftel.madisonmk.com/reports/user"
        user_id = my_set.get('user')
        webbrowser.open(report_uri + "/" + user_id)
        
        
# https://forum.sublimetext.com/t/simple-examples-for-textinputhandler/48422/13