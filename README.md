Description
===========

Trusted Coding Sublime is a package for [Sublime Text](http://www.sublimetext.com) to statically analyze code and find security flaws.

There are two methodologies used for this purpouse, [OWASP](https://owasp.org/) and [CWE](https://cwe.mitre.org/).

**The code will be sent to our backend where a bunch of tools will analyze, treat and return the results.**

Please, send your [Feedback](https://forms.office.com/Pages/ResponsePage.aspx?id=4lK_9BqGX0yiNN1CRzyGxXguJMU1G2VPqCi2TeKeC_1UNzM4N1RQRDJRVlM2RDhHSUkzS0QyOUtWRy4u)

Languages
===========
Different tools for different languages.

Python

* Semgrep
* Bandit

PHP

* Semgrep
* PHPCS
* PHPStan
* Parse
* Exakat

Installation
============

You can install this package through [Package Control](https://sublime.wbond.net/installation), simply use Command Palette: Package Control Install Package -> Trusted Coding.

Alternatively, you can install this package by running the following command in your Packages directory:

    git clone https://bitbucket.org/madnext/trustedcodingsublime.git

or

    git clone git@bitbucket.org:madnext/trustedcodingsublime.git

Usage
------------------------------------------------------------
All configurations can be modified in the context menu or in the command palette.

- **Deep Test**: Unchecked == Less tools. Checked == More tools.
- **Results in New File**: Display results in the same page or in a new one.
- **Security References**: Show OWASP and CWE references related with the current vulnerabilities.
- **Set Experience Time**: Set the number of years you have been developing.
- **Set Development Time**: Set the minutes you have been developing a script.
- **My Stats**: Show your usage data.

This menu also include the two main functions:
- **Start Analysis**: This will analyze your code.
- **Clear Results**: To clear the screen, click this option.

![Command Palette Options](docs/commandpalette.png)

Or in the main menu:

![Main Menu Options](docs/screenshot_0.PNG)

When the analysis finishes, this will display something like this:

![Trusted Coding Results](docs/screenshot_1.PNG)

You can close these Popups one by one by clicking on "Hide" or choose "Clear Warnings in Code" option in Main menu/Command Palette.

------------------------------------------------------------

# TERMINOS Y CONDICIONES TRUSTED CODING 

## 1. TRUSTED CODING 

Esta iniciativa es propiedad de:  

    TELECYL, S.A. (en adelante “MADISON MK”)
    Enrique Cubero 9 – Edificio Madison Arena
    47014 Valladolid
    Tel: 914 521 800
    CIF: A47310941

Es un proyecto financiado por el Ministerio de Economía y Empresa; en la convocatoria THD 1/2019 de ayudas en el ámbito de las Tecnologías Habilitadoras Digitales, dentro del Plan de Investigación Científica y Técnica y de Innovación 2017-2020 en el marco de la Acción Estratégica de Economía y Sociedad Digital y del Subprograma Estatal de impulso a las Tecnologías Habilitadoras. 

## 2. CONTENIDO DE LOS TÉRMINOS Y CONDICIONES DE USO 

### 2.1 Aceptación 

Usted debe leer estos Términos y condiciones antes de hacer uso del servicio. Al hacer uso del servicio acepta totalmente estas condiciones de uso. 

### 2.2 Condiciones sobre el uso del servicio 

Para hacer uso de este servicio debe tener en cuenta de forma previa las siguientes consideraciones: 

- Debe tener 18 años o más para utilizar el Servicio. Usted declara y garantiza que tiene al menos 18 años y es mayor de edad para celebrar un contrato vinculante. 
- Para hacer uso de este servicio, usted comprende que toda la información que desea analizar a través de este servicio es tratada por los procesos internos de análisis de vulnerabilidades con el objetivo de detectar vulnerabilidades o posibles vulnerabilidades e informarle de las mismas. 
- Usted conserva los derechos de propiedad de la información que ha decidido analizar en el servicio, no obstante, utilizando el servicio otorga una licencia para analizar mediante diversos procesos internos la información facilitada.   
- El objetivo de hacer uso de este servicio debe ser únicamente el de comprobar vulnerabilidades o posibles vulnerabilidades en su códigoproyecto con el fin de mejorar la seguridad de este. 
- Si está utilizando el Servicio en nombre de una empresa, nos declara que tiene autoridad para vincular esa empresa o entidad a estos Términos y que la empresa acepta estos Términos.   

Queda terminantemente prohibido usar este servicio: 

- Si la información que va a analizar contiene cualquier tipo de dato confidencial, sensible o cualquier tipo de dato personal y no dispone de la autorización expresa del propietario. 
- De cualquier manera, que viole cualquier ley o regulación local, nacional o internacional aplicable. 
- Si el objetivo de analizar las vulnerabilidades es la de cometer cualquier tipo de ataque. 
- Con el objetivo de atacar, bloquear o degradar la disponibilidad del servicio. 
- Con el objetivo de intentar obtener acceso no autorizado a cualquier información, servidores y comunicaciones que forman parte del servicio. 
- Con fines fraudulentos ni para llevar a cabo ninguna conducta que pueda dañar la imagen, los intereses y los derechos de MADISON MK o de terceros.  

### 2.3 Riesgos informáticos 

Este servicio no se responsabiliza de los daños y perjuicios causados como consecuencia de riesgos inherentes al medio empleado, ni de los ocasionados por las vulnerabilidades en tus sistemas y herramientas.  

Es tu responsabilidad disponer de sistemas actualizados de detección de software malicioso, parches de seguridad actualizados y tomar las precauciones necesarias para no caer en fraudes o piratería informática. 

### 2.4 Legislación 

Los presentes Términos y condiciones de uso se someterán a la legislación española. 

Cualquier diferencia o conflicto respecto a la interpretación o cumplimiento que surja con respecto a las mismas se dirimirá ante la jurisdicción de los Juzgados y Tribunales de Valladolid capital, con renuncia expresa a cualquier otro fuero que pudiera corresponderles. 